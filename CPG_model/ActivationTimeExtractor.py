# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

from copy import copy
import numpy as np
from scipy import signal

class ActivationTimeExtractor(object):
    ''' Class with methods to get from events to actuall chamber activation
    times per oscillator and frequency of oscillator '''

    def get_average_activitiy_trace(self, spike_times, dt, tau, A_step, t_start=None, t_end=None):
        ''' Sum up all spikes in one neuron and assume exponential decay in this neuron's activity
                All time information should be provided in us (1e6)

            spike_times (list/nparray): spike times used to create activity trace for
            dt (float): time step used in A
            tau (float): decay time constant for activity trace A
            A_step (float): increase in A per event
            t_start (float): beginning of spike times to look at (same unit as spike_times)
            t_end (float): end of spike times to look at (same unit as spike_times)
        '''

        n_spikes = len(spike_times)

        if (n_spikes > 0):
            # get dimensions of A and initialize A and A_t
            if t_start is None:
                t_start = 0.
            if t_end is None:
                t_end = np.max(spike_times)+dt
            t_start = float(t_start)
            t_end = float(t_end)
            A_t = np.arange(t_start, t_end, dt)
            A = np.zeros_like(A_t)

            # sort spike times and remove spikes before t_start
            spike_times = np.sort(spike_times)
            indices_considered_spike_times = np.where(np.logical_and(spike_times >= t_start,
                                                                     spike_times <= t_end))[0]
            spike_times = spike_times[indices_considered_spike_times]

            # add t_end as last timestep (will not be treated as event
            # but A-trace will decay until t_end then)
            spike_times = np.concatenate((spike_times, [t_end]))

            # find first spike and keep everything at zeros until first spike, then add + A_step
            t_step_nr = int(np.floor((spike_times[0] - t_start) / dt))
            A[t_step_nr] = A_step

            # run over spike times until reaching spike_times larger than t_end
            for spike_time_nr, event_time in enumerate(spike_times[:-1]):
                spike_time_start = event_time - t_start
                spike_time_end   = spike_times[spike_time_nr + 1] - t_start

                t_step_nr_start = int(np.floor((spike_time_start - t_start) / dt))
                t_step_nr_end = int(np.floor((spike_time_end - t_start) / dt))
                n_timestep_inbetween = t_step_nr_end - t_step_nr_start

                # add spike to A and add previous state of A minus decay of one time step
                A[t_step_nr_start] = A_step + A[t_step_nr_start - 1] * np.exp(-dt / tau)
                if n_timestep_inbetween > 1:
                    # get exp decay until timestep before next spike
                    ts = np.linspace(1, n_timestep_inbetween, n_timestep_inbetween - 1) * dt
                    A[t_step_nr_start + 1:t_step_nr_end] = A[t_step_nr_start] * np.exp(- ts / tau)  # removed * dt

        else:
            if (t_start is not None) and (t_end is not None):
                A_t = np.arange(t_start, t_end, dt)
            else:
                A_t = np.zeros([1, 1])
            A = np.zeros_like(A_t)

        return A, A_t


    def get_t_threshold_crossing(self, A, A_t, thr, min_threshold_btw_peaks=np.inf,
                                 min_t_diff=None):
        '''
        A (list/nparray): activation trace of one population
        A_t (list/nparray): time points matching A
        thr (float): threshold value
        min_threshold_btw_peaks (float): minimal value that A has to get below to separate two peaks
        min_t_diff (float): minimal time difference between two peaks to consider them two separate peaks
        '''

        # look for locations with value above threshold
        indices_above_thr = np.where(A > thr)[0]
        # check diff between indices found, assuming that indices which are adjacent are part of the same bumb, hence
        # only indices with diff>1 describe beginning / crossing of new bump
        if len(indices_above_thr)>0:
            diff_indices_above_thr = np.diff(indices_above_thr)
            indices_thr_crossing = [indices_above_thr[0]] + list(indices_above_thr[np.where(diff_indices_above_thr > 1)[0]+1])
        else:
            indices_thr_crossing = []


        # check that signal has gone down (below thr_min) between peaks
        if min_threshold_btw_peaks == np.inf:
            true_indices_thr_crossing = indices_thr_crossing
        else:
            # get max between two zero-crossings (assumption: curve goes always back to zero (bzw min_threshold_btw_peaks)
            # before it goes up again)
            prev_peak_index = 0
            peak_group_index_to_A, true_indices_thr_crossing = {}, []
            for peak_index in indices_thr_crossing:
                indices_zeros = np.where(A[prev_peak_index:peak_index] <= min_threshold_btw_peaks)[0]
                if len(indices_zeros) == 0:
                    peak_group_index_to_A[peak_index] = A[peak_index]
                elif len(indices_zeros) > 0:
                    if len(peak_group_index_to_A.keys()) > 0:
                        true_indices_thr_crossing.append(
                            max(peak_group_index_to_A, key=peak_group_index_to_A.get))
                        peak_group_index_to_A = {}
                        peak_group_index_to_A[peak_index] = A[peak_index]
                    else:
                        peak_group_index_to_A[peak_index] = A[peak_index]
                prev_peak_index = copy(peak_index)

            # to extract last peak as well
            if len(peak_group_index_to_A) > 0:
                true_indices_thr_crossing.append(max(peak_group_index_to_A, key=peak_group_index_to_A.get))

        t_thr_crossing = A_t[true_indices_thr_crossing]

        if min_t_diff is not None:
            # remove t_thr where thr crossing too close to each other (always remove second peak)
            t_diffs = np.diff(t_thr_crossing)
            if any(t_diffs < min_t_diff):
                indices_t_adj_thr_crossing = np.where(t_diffs < min_t_diff)[0]
                for idx in indices_t_adj_thr_crossing:
                    assert ((t_thr_crossing[idx] - t_thr_crossing[idx + 1]) < min_t_diff)
                    t_thr_crossing[idx + 1] = t_thr_crossing[idx]
            t_thr_crossing = np.unique(t_thr_crossing)

        return t_thr_crossing


    def remove_double_peaks(self, activation_times_exc, activation_times_inh):

        remaining_activation_times_exc = []
        remaining_activation_times_inh = []

        # comes with removing all the exc spikes which happen before the first inh spike

        # remove all double exc spikes between two inh spikes
        for t_thr_inh_nr in range(len(activation_times_inh[:-1])):

            t_thr_exc_indices_within_interval = \
            np.where(np.logical_and(activation_times_exc > activation_times_inh[t_thr_inh_nr],
                                    activation_times_exc < activation_times_inh[t_thr_inh_nr + 1]))[0]
            t_thr_exc_within_interval = activation_times_exc[t_thr_exc_indices_within_interval]

            if len(t_thr_exc_indices_within_interval) == 1:
                remaining_activation_times_exc.extend([t_thr_exc_within_interval])
            elif len(t_thr_exc_indices_within_interval) > 1:
                remaining_activation_times_exc.extend([np.min(t_thr_exc_within_interval)])

        # remove all double inh spikes between two exc spikes
        for t_thr_exc_nr in range(len(remaining_activation_times_exc[:-1])):

            t_thr_inh_indices_within_interval = \
            np.where(np.logical_and(activation_times_inh > remaining_activation_times_exc[t_thr_exc_nr],
                                    activation_times_inh < remaining_activation_times_exc[t_thr_exc_nr + 1]))[0]
            t_thr_inh_within_interval = activation_times_inh[t_thr_inh_indices_within_interval]

            if len(t_thr_inh_indices_within_interval) == 1:
                remaining_activation_times_inh.extend([t_thr_inh_within_interval])
            elif len(t_thr_inh_indices_within_interval) > 1:
                remaining_activation_times_inh.extend([np.min(t_thr_inh_within_interval)])


        # check if correct
        for t_thr_nr, t_thr_inh in enumerate(remaining_activation_times_inh[:-1]):
            t_start = t_thr_inh
            t_end = remaining_activation_times_inh[t_thr_nr + 1]

            idx_t_thr_exc_within_interval = np.where(np.logical_and(remaining_activation_times_exc > t_start,
                                                                    remaining_activation_times_exc < t_end))[0]
            assert len(idx_t_thr_exc_within_interval) == 1

        for t_thr_nr, t_thr_exc in enumerate(remaining_activation_times_exc[:-1]):
            t_start = t_thr_exc
            t_end = remaining_activation_times_exc[t_thr_nr + 1]

            idx_t_thr_inh_within_interval = np.where(np.logical_and(remaining_activation_times_inh > t_start,
                                                                    remaining_activation_times_inh < t_end))[0]
            assert len(idx_t_thr_inh_within_interval) == 1

        return np.asarray(remaining_activation_times_exc), np.asarray(remaining_activation_times_inh)


    def get_delays_between_two_populations(self, activation_times_popA,
                                           activation_times_popB, check_alternation):


        if len(activation_times_popA) == 0 or len(activation_times_popB)==0:
            dt_popA_to_popB = np.array([0])
            dt_popB_to_popA = np.array([0])

        else:
            if check_alternation:
                # adjust provided activation times s.t. A-B comes only in couples (remove early B, and late A)
                # check if starts with an A
                if activation_times_popB[0] < activation_times_popA[0]:
                    adjusted_activation_times_popB = activation_times_popB[1:]
                else:
                    adjusted_activation_times_popB = activation_times_popB
                if activation_times_popA[-1] > activation_times_popB[-1]:
                    adjusted_activation_times_popA = activation_times_popA[:-1]
                else:
                    adjusted_activation_times_popA = activation_times_popA

                if len(adjusted_activation_times_popA) != len(adjusted_activation_times_popB):
                    raise Warning("activation of popA and popB are not alternating")
                else:
                    dt_popA_to_popB = adjusted_activation_times_popB - adjusted_activation_times_popA
                    dt_popB_to_popA = adjusted_activation_times_popA[1:] - adjusted_activation_times_popB[:-1]

                    if (dt_popB_to_popA<0).any() or (dt_popA_to_popB<0).any():
                        raise Warning("activation of popA and popB are not alternating")


            else: #  do not check whether the order of A and B is always the same
                # get dt_popA_to_popB
                dt_popA_to_popB = []
                for t_spike_popA in activation_times_popA:
                    activation_times_popB_afterwards = activation_times_popB[activation_times_popB>t_spike_popA]
                    if len(activation_times_popB_afterwards)>0:
                        dt_popA_to_popB.append(np.min(activation_times_popB_afterwards) - t_spike_popA)
                dt_popB_to_popA = []
                for t_spike_popB in activation_times_popB:
                    activation_times_popA_afterwards = activation_times_popA[activation_times_popA>t_spike_popB]
                    if len(activation_times_popA_afterwards)>0:
                        dt_popB_to_popA.append(np.min(activation_times_popA_afterwards) - t_spike_popB)
                dt_popA_to_popB = np.asarray(dt_popA_to_popB)
                dt_popB_to_popA = np.asarray(dt_popB_to_popA)

        return dt_popA_to_popB, dt_popB_to_popA
