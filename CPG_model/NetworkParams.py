# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.MyDynapseNeuron import MyDynapseNeuron

class NetworkParams(object):
    ''' Class used as data structure to store all network parameters '''

    def __init__(self, CtxDynapse, N_osc=3):
        # DEFINE WHERE TO PLACE POPULATIONS
        self.neurons_per_core = 256
        self.cores_per_chip = 4

        self.N_osc = N_osc

        self.N_exc = [16] * self.N_osc
        self.N_inh = [4] * self.N_osc

        if self.N_osc == 3:
            self.chip_ids_pop_exc = [0, 0, 2]
            self.chip_ids_pop_inh = [0, 0, 2]

            self.core_ids_chiplevel_pop_exc = [0, 2, 0]
            self.core_ids_chiplevel_pop_inh = [1, 3, 1]
        elif self.N_osc == 2:
            self.chip_ids_pop_exc = [0, 0]
            self.chip_ids_pop_inh = [0, 0]

            self.core_ids_chiplevel_pop_exc = [0, 2]
            self.core_ids_chiplevel_pop_inh = [1, 3]

        self.core_ids_global_pop_exc, self.core_ids_global_pop_inh = [], []
        for osc_nr in range(self.N_osc):
            self.core_ids_global_pop_exc.append(self.cores_per_chip * self.chip_ids_pop_exc[osc_nr]
                                                + self.core_ids_chiplevel_pop_exc[osc_nr])
            self.core_ids_global_pop_inh.append(self.cores_per_chip * self.chip_ids_pop_inh[osc_nr]
                                                + self.core_ids_chiplevel_pop_inh[osc_nr])
        self.core_ids_global_in_use = self.core_ids_global_pop_exc + self.core_ids_global_pop_inh


        self.active_neuron_ids_chiplevel_per_core = {}
        for core_id_global in self.core_ids_global_in_use:
            self.active_neuron_ids_chiplevel_per_core[core_id_global] = []


        self.MyDynNeurons_start_exc, self.MyDynNeurons_start_inh = [], []
        self.neuron_ids_global_start_exc, self.neuron_ids_global_start_inh = [], []
        self.neuron_ids_global_pops_exc, self.neuron_ids_global_pops_inh = [], []
        for osc_nr in range(self.N_osc):
            if osc_nr == 2:
                self.MyDynNeurons_start_exc.append(MyDynapseNeuron(chip_id=self.chip_ids_pop_exc[osc_nr],
                                                              core_id=self.core_ids_chiplevel_pop_exc[osc_nr],
                                                              column_nr_core=0,
                                                              row_nr_core=12))
                self.MyDynNeurons_start_inh.append(MyDynapseNeuron(chip_id=self.chip_ids_pop_inh[osc_nr],
                                                              core_id=self.core_ids_chiplevel_pop_inh[osc_nr],
                                                              column_nr_core=6,
                                                              row_nr_core=12))
            else:
                self.MyDynNeurons_start_exc.append(MyDynapseNeuron(chip_id=self.chip_ids_pop_exc[osc_nr],
                                                              core_id=self.core_ids_chiplevel_pop_exc[osc_nr],
                                                              column_nr_core=0,
                                                              row_nr_core=8))
                self.MyDynNeurons_start_inh.append(MyDynapseNeuron(chip_id=self.chip_ids_pop_inh[osc_nr],
                                                              core_id=self.core_ids_chiplevel_pop_inh[osc_nr],
                                                              column_nr_core=6,
                                                              row_nr_core=8))
            self.neuron_ids_global_start_exc.append(self.MyDynNeurons_start_exc[osc_nr].neuron_id_global)
            self.neuron_ids_global_start_inh.append(self.MyDynNeurons_start_inh[osc_nr].neuron_id_global)
            self.neuron_ids_global_pops_exc.append(list(range(self.MyDynNeurons_start_exc[osc_nr].neuron_id_global,
                                                              self.MyDynNeurons_start_exc[osc_nr].neuron_id_global+self.N_exc[osc_nr])))
            self.neuron_ids_global_pops_inh.append(list(range(self.MyDynNeurons_start_inh[osc_nr].neuron_id_global,
                                                              self.MyDynNeurons_start_inh[osc_nr].neuron_id_global+self.N_inh[osc_nr])))
            core_id_global_exc = self.core_ids_global_pop_exc[osc_nr]
            core_id_global_inh = self.core_ids_global_pop_inh[osc_nr]
            self.active_neuron_ids_chiplevel_per_core[core_id_global_exc].extend(list(range(self.MyDynNeurons_start_exc[osc_nr].neuron_id_chip,
                                                                                            self.MyDynNeurons_start_exc[osc_nr].neuron_id_chip+self.N_exc[osc_nr])))
            self.active_neuron_ids_chiplevel_per_core[core_id_global_inh].extend(list(range(self.MyDynNeurons_start_inh[osc_nr].neuron_id_chip,
                                                                                            self.MyDynNeurons_start_inh[osc_nr].neuron_id_chip+self.N_inh[osc_nr])))
        # SETUP NETWORK
        self.random_seed = 12
        self.syntypes_exc2exc = [CtxDynapse.DynapseCamType.SLOW_EXC]*self.N_osc
        self.syntypes_exc2inh = [CtxDynapse.DynapseCamType.FAST_EXC]*self.N_osc
        self.syntypes_inh2exc = [CtxDynapse.DynapseCamType.SLOW_INH]*self.N_osc
        self.syntypes_excA2excB = [CtxDynapse.DynapseCamType.FAST_EXC]*self.N_osc
        self.syntypes_inhA2inhB = [CtxDynapse.DynapseCamType.SLOW_INH]*self.N_osc
        self.syntypes_excA2inhB = [CtxDynapse.DynapseCamType.SLOW_EXC]*self.N_osc
        self.syntypes_inhA2excB = [CtxDynapse.DynapseCamType.FAST_INH]*self.N_osc

        # n_conn = n_presynaptic_partners per neuron
        self.n_conn_exc2exc = [15]*self.N_osc
        self.n_conn_exc2inh = [16]*self.N_osc
        self.n_conn_inh2exc = [4]*self.N_osc
        self.n_conn_excA2excB = [16]*self.N_osc
        self.n_conn_inhA2inhB = [4]*self.N_osc
        self.n_conn_excA2inhB = [16]*self.N_osc
        self.n_conn_inhA2excB = [4]*self.N_osc

        self.interosc_connection_types=['excA2excB', 'inhA2inhB']
        self.conn_strength_exc2exc=[1]*self.N_osc
        self.conn_strength_exc2inh=[1]*self.N_osc
        self.conn_strength_inh2exc=[4]*self.N_osc
        self.conn_strength_excA2excB=[2]*self.N_osc
        self.conn_strength_inhA2inhB=[4]*self.N_osc
        self.conn_strength_excA2inhB=[1]*self.N_osc
        self.conn_strength_inhA2excB=[1]*self.N_osc






