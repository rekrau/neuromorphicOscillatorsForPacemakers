# Neuromorphic Oscillators for Pacemakers
Here we provide the code to the paper "Robust neuromorphic coupled oscillators for adaptive pacemakers".
This code allows you to implement a system of three coupled oscillators on the DYNAP-SE board and tune their frequency and phase shift.
It also shows you how to build and tune the parameters to provide an inhibitory input signal to adapt the oscillators' frequencies to implement an adaptive pacemaker.

Additionally, this repository contains the DYNAP-SE biases used to generated the data shown in the paper (bias_sets/dynapse_biases.json) and the sECG and respiratory signal data of dogs at rest used to fit the model of the adaptive cardiac pacemaker (ECG/recording_sECG_and_respiratorySignal.xlsx).


## Installation
### Prerequisites
* contrexcontrol v4.0.2 (now samna, see: https://pypi.org/project/samna/)
* brian2 (https://brian2.readthedocs.io/en/stable/introduction/install.html)
* teili (https://teili.readthedocs.io/en/latest/)
* biosppy (https://biosppy.readthedocs.io/en/stable/)
```bash
pip install brian2
pip install teili
pip install biosppy
```

## Usage
To set up a system of 3 coupled oscillators and tune it as described in the paper, please use the scripts in this order:
* ***01_tune_three_coupled_oscillators.ipynb***
  Script to set up and tune the frequency and phase shift of a system of 3 coupled oscillators
* ***02_run_param_sweep_DCInput.ipynb***
Script to run parameter swipe over DC on each individual oscillator to later obtain explicit function to set oscillation frequency
* ***03_fit_fct_on_DCInput_to_frequency.ipynb***
Script to fit function on previously obtained data (run_param_swipe_constFreq.ipynb) to set oscillation frequency explicitly
* ***04_eval_fittedFct_on_DCInput_to_frequency.ipynb***
Script to evaluate how well the previously fitted function (to set oscillation frequency explicitly) works
* ***05_analysis_sECG_and_respiratorySignal.ipynb***
Script to look at recorded sECG and respiratory signal data. It also shows how the breathing coefficient is calculated and how the relation between the R-R interval and the average breathing coefficient is obtained.
* ***06_run_param_sweep_on_inhInputSpikeRate_for_RSA.ipynb***
Script to run swipe on each oscillator over a set of inhibitory input spike frequencies. This is later used to obtain an initial guess of how to set the inhibitory input strength to adapt the heart rate to the respiratory signal (RSA restoration).
* ***07_tune_three_coupled_oscillators_on_rsa_restoration.ipynb***
Script to tune a system of three coupled oscillators to adapt the oscillation frequency to model the RSA present in sECG recordings of dogs at rest.

## License
 [![CC BY 4.0][cc-by-shield]][cc-by]
The data collected in this work (recording_sECG_and_respiratorySignal.xlsx) is licensed under a [Creative Commons Attribution 4.0 International License][cc-by].
[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

The provided code is licenced under the MIT license, see the LICENSE_MIT file.
