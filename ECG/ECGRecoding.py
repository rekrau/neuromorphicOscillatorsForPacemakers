# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import matplotlib.pylab as plt
from biosppy.signals import ecg
from scipy import signal

from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.function_templates import logistic_curve


class ECGRecording(object):
    ''' class to store ECG data and lung inflation data.
    Contains methods used to generate breathing coefficient.
    '''

    def __init__(self, dog_nr, gender, path_to_rec, exp_date, duration=None,
                 t_rec=None, sampling_rate=100., RA_trace=None, RV_trace=None, LV_trace=None,
                 BP=None, lung_inflation=None, activity=None, sECG_trace=None):
        """
         dog_nr (int): number of dog which provided recording
         gender ('male'/'female'): gender of dog which provided recording
         path_to_rec (str): path to recording used
         exp_date (int): date of recording as JJJJMMDD
         duration (float): duration of recording in seconds
         t_rec (list/nparray): recording time points
         sampling_rate (float): sampling frequency of recording
         RA_trace/RV_trace/LV_trace/BP/lung_inflation/activity/sECG_trace (list/nparray): different data arrays


        """
        self.dog_nr = dog_nr
        self.gender = gender
        self.path_to_rec = path_to_rec
        self.exp_date = exp_date

        self.duration = duration  # seconds
        self.sampling_rate = sampling_rate  # Hz

        self.t_rec = t_rec
        self.RA_trace = RA_trace
        self.RV_trace = RV_trace
        self.LV_trace = LV_trace
        self.blood_pressure = BP
        self.lung_inflation = lung_inflation
        self.activity = activity
        self.sECG_trace = sECG_trace

    def restrict_ECGRecording_to_range(self, idx_start, idx_end, names_traces):
        for trace_name in names_traces:
            restricted_trace = getattr(self, trace_name)[idx_start:idx_end]
            setattr(self, trace_name, restricted_trace)

    def _create_subfig_if_None(self, subfig):
        if subfig == None:
            fig, subfig = plt.subplots(figsize=(20, 5))
        return subfig

    def _set_labels(self, title, xlabel, ylabel, subfig, fontsize):

        if title is not None:
            subfig.set_title(title, fontsize=fontsize)
        if xlabel is not None:
            subfig.set_xlabel(xlabel, fontsize=fontsize)
        if ylabel is not None:
            subfig.set_ylabel(ylabel, fontsize=fontsize)

    def plot_trace(self, trace_name, t_start=None, t_end=None, title=None,
                   xlabel=None, ylabel=None, subfig=None, color='k', fontsize=12):

        subfig = self._create_subfig_if_None(subfig=subfig)
        self._set_labels(title=title, xlabel=xlabel, ylabel=ylabel, subfig=subfig, fontsize=fontsize)

        trace_to_show = getattr(self, trace_name)

        if self.t_rec is None:
            # if t_rec is not provided, t_start, t_end are treated as indices for trace_to_show
            idx_start, idx_end = t_start, t_end
            if idx_start is None:
                idx_start = 0
            if idx_end is None:
                idx_end = len(trace_to_show)
            subfig.plot(range(idx_start,idx_end), trace_to_show[idx_start:idx_end], color=color)

        else:
            if t_start is None:
                t_start = 0
            if t_end is None:
                t_end = self.t_rec[-1]
            idx_to_show = np.logical_and(self.t_rec>t_start, self.t_rec<t_end)
            subfig.plot(self.t_rec[idx_to_show], trace_to_show[idx_to_show], color=color)

        return subfig

    def plot_extracted_peaks(self, peak_name, t_start=None, t_end=None, title=None,
                             xlabel=None, ylabel=None, subfig=None, color='k', fontsize=12):

        subfig = self._create_subfig_if_None(subfig=subfig)
        self._set_labels(title=title, xlabel=xlabel, ylabel=ylabel, subfig=subfig, fontsize=fontsize)

        peaks_to_plot = getattr(self, peak_name)

        if t_start is None:
            t_start = 0
        if t_end is None:
            t_end = np.inf

        label = peak_name
        for t_peak in peaks_to_plot:
            t_peak = self.t_rec[t_peak]
            if t_peak > t_start and t_peak < t_end:
                subfig.axvline(x=t_peak, color=color, linestyle=':', linewidth=1,
                               label=label)
                label = '_nolegend_'
        return subfig

    def extract_rpeaks(self):
        _ts, self.filtered_sECG_trace, self.rpeaks_indices, _templates_time_axis_ref, \
        _extracted_heartbeat_templates, _heart_rate_time_axis_ref, \
        self.heart_rate = ecg.ecg(signal=self.sECG_trace,
                                  sampling_rate=self.sampling_rate,
                                  show=False)
        self._set_rpeaks_widths()

    def _set_rpeaks_widths(self):
        """
        widths: ndarray - The widths for each peak in samples.
        width_heights: ndarray - The height of the contour lines at which the
            widths where evaluated.
        left_ips, right_ips: -nddarray - Interpolated positions of left and
            right intersection points of a horizontal line at the respective
            evaluation height.
        """
        wlen = 150. # np.max(np.diff(self.rpeaks_indices))*0.

        self.rpeaks_widths, self.rpeak_width_heights, self.rpeak_left_ips, self.rpeak_right_ips = [], [], [], []
        for rpeak_nr, rpeak_idx in enumerate(self.rpeaks_indices):
            wlen = np.mean(np.diff(self.rpeaks_indices[np.max((0, rpeak_nr-1)):np.min((len(self.rpeaks_indices), rpeak_nr+2))])) * 0.5
            rpeaks_width, rpeak_width_height, rpeak_left_ip, rpeak_right_ip = signal.peak_widths(x=self.filtered_sECG_trace,
                                                                                                 peaks=[rpeak_idx], rel_height=1.0, wlen=wlen)
            self.rpeaks_widths.append(rpeaks_width)
            self.rpeak_width_heights.append(rpeak_width_height)
            self.rpeak_left_ips.append(rpeak_left_ip)
            self.rpeak_right_ips.append(rpeak_right_ip)

        self.rpeaks_widths = np.asarray(self.rpeaks_widths)
        self.rpeak_width_heights = np.asarray(self.rpeak_width_heights)
        self.rpeak_left_ips = np.asarray(self.rpeak_left_ips)
        self.rpeak_right_ips = np.asarray(self.rpeak_right_ips)

    def extract_ppeaks(self):
        if not hasattr(self, 'rpeaks_indices'):
            self.extract_rpeaks()

        # extract and set ppeaks and their peak-widths
        self.ppeaks_indices, self.ppeaks_widths = [], []
        for rpeak_nr in range(1, len(self.rpeaks_indices)):

            prev_rpeak_idx    = self.rpeaks_indices[rpeak_nr-1]
            current_rpeak_idx = self.rpeaks_indices[rpeak_nr]
            inter_peak_dist   = current_rpeak_idx - prev_rpeak_idx

            factor_inter_peak_dist = 0.5
            ppeaks = []
            while len(ppeaks) == 0:
                factor_inter_peak_dist += 0.05  # arbitrary step size to increase distance considered to find peak
                start_idx = int(current_rpeak_idx - (inter_peak_dist*factor_inter_peak_dist))
                end_idx   = int(current_rpeak_idx - self.rpeaks_widths[rpeak_nr]*(0.5+0.1))

                relevant_signal = self.filtered_sECG_trace[start_idx:end_idx]
                ppeaks, ppeak_props = signal.find_peaks(x=relevant_signal,
                                                        height=0,
                                                        distance=abs(end_idx-start_idx)+2,
                                                        width=(0, len(relevant_signal)))

            self.ppeaks_indices.append(ppeaks[0]+start_idx)

            peak_width = ppeak_props['right_ips'][0] - ppeak_props['left_ips'][0]
            self.ppeaks_widths.append(peak_width)

        wlen = 100.  # np.max(np.diff(self.rpeaks_indices))*0.
        _, self.ppeak_width_heights, self.ppeak_left_ips, \
        self.ppeak_right_ips = signal.peak_widths(x=self.filtered_sECG_trace,
                                                  peaks=self.ppeaks_indices,
                                                  rel_height=1.0, wlen=wlen)

    def extract_p_and_r_peaks(self):
        self.extract_rpeaks()
        self.extract_ppeaks()
    #
    def extract_lunginflation_peaks(self, peak_dist=100., peak_width=100.):
        # increase in value, means decrease in volume --> expiration u.u.
        self.lipeaks_indices_insp, _ =  signal.find_peaks(x=self.lung_inflation*-1.,
                                                  distance=peak_dist, width=peak_width)
        self.lipeaks_indices_exp, _ =  signal.find_peaks(x=self.lung_inflation,
                                                 distance=peak_dist, width=peak_width)

        self.lipeaks_indices_start_insp = self.lipeaks_indices_exp  # peak of exp, is start of insp
        self.lipeaks_indices_start_exp  = self.lipeaks_indices_insp  # peak of insp, is start of exp

    def set_expiration_phase_trace(self):

        self.exp_phase = np.zeros_like(self.t_rec, dtype=int)

        # set peak_nr for insp and exp so that idx_insp_peak of peak_nr_insp is smaller than idx_peak_exp of peak_nr_exp
        peak_nr_start_insp = 0
        if self.lipeaks_indices_start_insp[0] < self.lipeaks_indices_start_exp[0]:  # first peak is for start of insp, then mark initial
            # section as expiration
            self.exp_phase[:self.lipeaks_indices_start_insp[0]] = 1
            peak_nr_start_exp = 0
            n_breaths = np.min((len(self.lipeaks_indices_start_insp)-1, len(self.lipeaks_indices_start_exp)))
        else:  # first peak is for start of expiration
            self.exp_phase[self.lipeaks_indices_start_exp[0]:self.lipeaks_indices_start_insp[0]] = 1
            peak_nr_start_exp = 1
            n_breaths = np.min((len(self.lipeaks_indices_start_insp)-1, len(self.lipeaks_indices_start_exp)-1))

        # set trace
        for breath_nr in range(n_breaths):
            idx_start_exp = self.lipeaks_indices_start_exp[peak_nr_start_exp]
            idx_end_exp   = self.lipeaks_indices_start_insp[peak_nr_start_insp+1]
            self.exp_phase[idx_start_exp:idx_end_exp] = 1

            peak_nr_start_exp += 1
            peak_nr_start_insp += 1

        # define last part of trace
        if self.lipeaks_indices_start_insp[-1] < self.lipeaks_indices_start_exp[-1]:
            self.exp_phase[self.lipeaks_indices_start_exp[-1]:] = 1


    def set_inspiration_phase_trace(self):

        self.insp_phase = np.zeros_like(self.t_rec, dtype=int)
        # set peak_nr for insp and exp so that idx_insp_peak of peak_nr_insp is smaller than idx_peak_exp of peak_nr_exp
        peak_nr_start_insp = 0
        if self.lipeaks_indices_start_insp[0] < self.lipeaks_indices_start_exp[0]:  # first peak is for start of insp, then mark initial
            # section as expiration
            peak_nr_start_exp = 0
            n_breaths = np.min((len(self.lipeaks_indices_start_insp), len(self.lipeaks_indices_start_exp)))
        else:  # first peak is for start of expiration
            self.insp_phase[0:self.lipeaks_indices_start_exp[0]] = 1
            peak_nr_start_exp = 1
            n_breaths = np.min((len(self.lipeaks_indices_start_insp), len(self.lipeaks_indices_start_exp)-1))

        # set trace
        for breath_nr in range(n_breaths):
            idx_start_insp = self.lipeaks_indices_start_insp[peak_nr_start_insp]
            idx_end_insp   = self.lipeaks_indices_start_exp[peak_nr_start_exp]
            self.insp_phase[idx_start_insp:idx_end_insp] = 1

            peak_nr_start_exp += 1
            peak_nr_start_insp += 1

        # define last part of trace
        if self.lipeaks_indices_start_insp[-1] > self.lipeaks_indices_start_exp[-1]:
            self.insp_phase[self.lipeaks_indices_start_insp[-1]:] = 1


    def set_breathing_coefficient_trace(self, b, c, d, e):

        dt = float(1./self.sampling_rate)

        # trace EXP
        traces_exp = np.zeros_like(self.t_rec)
        for peak_nr_exp in range(len(self.lipeaks_indices_start_exp)):
            idx_start_exp = self.lipeaks_indices_start_exp[peak_nr_exp]
            if peak_nr_exp < len(self.lipeaks_indices_start_exp)-1:
                idx_next_start_exp = self.lipeaks_indices_start_exp[peak_nr_exp+1]
            else:
                idx_next_start_exp = len(traces_exp)  # last section
            n_timesteps_trace_up = int(idx_next_start_exp - idx_start_exp)

            trace_up = logistic_curve(np.arange(0, n_timesteps_trace_up*dt, dt), b, c, d, e)
            trace_up = np.clip(trace_up, a_min=0, a_max=np.inf)
            traces_exp[idx_start_exp:idx_next_start_exp] = trace_up

        # trace INSP
        traces_insp = np.zeros_like(self.t_rec)
        for peak_nr_insp in range(len(self.lipeaks_indices_start_insp)):
            idx_start_insp = self.lipeaks_indices_start_insp[peak_nr_insp]
            if peak_nr_exp < len(self.lipeaks_indices_start_insp)-1:
                idx_next_start_insp = self.lipeaks_indices_start_insp[peak_nr_insp+1]
            else:
                idx_next_start_insp = len(traces_insp)  # last section
            n_timesteps_trace_down = int(idx_next_start_insp - idx_start_insp)

            # trace_down = traces_exp[idx_start_insp] - alpha_insp*np.arange(0, n_timesteps_trace_down*dt, dt)
            trace_down = logistic_curve(np.arange(0, n_timesteps_trace_down*dt, dt), -b, c, d, e)
            offset = traces_exp[idx_start_insp-1]-np.max(trace_down)
            trace_down += offset
            trace_down = np.clip(trace_down, a_min=0, a_max=np.inf)
            traces_insp[idx_start_insp:idx_next_start_insp] = trace_down

        # multiply with exp/insp_phase to mask insp/exp phase
        self.breathing_coefficient_trace = traces_exp*self.exp_phase + traces_insp*self.insp_phase
        return self.breathing_coefficient_trace


    def get_dataset_breathing_coeff_vs_r_peak_delays(self, rpeaks_indices, breathing_coefficient_trace, dt):

        breathing_coeffs, r_peak_delays = [], []
        for r_peak_nr in range(len(rpeaks_indices) - 1):
            idx_r_peak_pre  = int(rpeaks_indices[r_peak_nr])
            idx_r_peak_post = int(rpeaks_indices[r_peak_nr + 1])
            dt_r_to_r = (idx_r_peak_post - idx_r_peak_pre) * dt
            r_peak_delays.append(dt_r_to_r)

            b_coeff = np.mean(breathing_coefficient_trace[idx_r_peak_pre:idx_r_peak_post])
            breathing_coeffs.append(b_coeff)

        return np.asarray(breathing_coeffs), np.asarray(r_peak_delays)


    def get_rpeak_ISIs_around_lipeaks(self, lipeak_type, offset_lipeaks=0,
                                      min_dt_rpeak_to_lipeak=0):
        # lipeak_type = 'exp' or 'insp'
        if lipeak_type == 'exp':
            lipeak_indices = self.lipeaks_indices_exp
        elif lipeak_type == 'insp':
            lipeak_indices = self.lipeaks_indices_insp
        else:
            raise Exception('lipeak_type {} unknown'.format(lipeak_type))

        # define dt (based on t_rec) (step size time)
        if self.t_rec is None:
            dt = 1.
        else:
            dt = np.abs(np.mean(np.diff(self.t_rec)))

        ISIs_lipeaks = []
        rpeaks_pre, rpeaks_post = [], []
        for lipeak_idx in lipeak_indices:
            for rpeak_idx_nr, rpeak_idx in enumerate(self.rpeaks_indices):
                if rpeak_idx > (lipeak_idx-offset_lipeaks) and rpeak_idx_nr!=0:

                    rpeak_pre  = self.rpeaks_indices[rpeak_idx_nr-1]
                    # if rpeak_pre is very close to peak of LI, take heart beat before this one
                    # if it is the first rpeak then there is no preceeding rpeak to take so just take this one
                    if np.abs(rpeak_pre*dt - lipeak_idx*dt) <= min_dt_rpeak_to_lipeak and rpeak_idx_nr>1:
                        rpeak_pre  = self.rpeaks_indices[rpeak_idx_nr-2]
                        rpeak_post = self.rpeaks_indices[rpeak_idx_nr-1]
                    else:
                        rpeak_pre  = self.rpeaks_indices[rpeak_idx_nr-1]
                        rpeak_post = self.rpeaks_indices[rpeak_idx_nr]

                    rpeaks_pre.append(rpeak_pre)
                    rpeaks_post.append(rpeak_post)
                    ISIs_lipeaks.append((rpeak_post-rpeak_pre)*dt)
                    del rpeak_pre, rpeak_post
                    break

        return ISIs_lipeaks, rpeaks_pre, rpeaks_post
