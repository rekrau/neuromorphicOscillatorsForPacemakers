# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import h5py

from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrEvents import get_events_within_dt
from neuromorphicOscillatorsForPacemakers.utils.utils_visualization.OscillatorPlotter import OscillatorPlotter

OPlt = OscillatorPlotter()


class OscEventsSaver(object):
    """ Class to save DYNAP-SE events to hdf5 files sorted by population of individual oscillators """

    def save_events(self, dt_init, dt_run,  outputfilename,
                    N_osc, neuron_ids_global_pops_exc, neuron_ids_global_pops_inh, BuffEF_all,
                    CtxDynapse, verbose=True):


        all_events = get_events_within_dt(dt_init=dt_init, dt_run=dt_run, current_EventBuffer=BuffEF_all, CtxDynapse=CtxDynapse, verbose=verbose)
        MyEMs = OPlt.events_to_EMs_of_coupled_oscs(all_events, N_osc, neuron_ids_global_pops_exc, neuron_ids_global_pops_inh)
        print('Data written to: ', outputfilename)

        h5_file = h5py.File(outputfilename, 'w')
        for osc_nr in range(N_osc):
            dset = h5_file.create_dataset("Osc{}/neuron_ids/exc".format(osc_nr), data=np.asarray(MyEMs['exc{}'.format(osc_nr)].neuron_ids), compression="gzip")
            dset = h5_file.create_dataset("Osc{}/neuron_ids/inh".format(osc_nr), data=np.asarray(MyEMs['inh{}'.format(osc_nr)].neuron_ids), compression="gzip")
            dset = h5_file.create_dataset("Osc{}/spiketimes/exc".format(osc_nr), data=np.asarray(MyEMs['exc{}'.format(osc_nr)].spike_times), compression="gzip")
            dset = h5_file.create_dataset("Osc{}/spiketimes/inh".format(osc_nr), data=np.asarray(MyEMs['inh{}'.format(osc_nr)].spike_times), compression="gzip")
        h5_file.close()

        return MyEMs

    def save_events_RSA(self, outputfilename,
                        N_osc, n_recs_required, all_MyEMs, all_dt_runs,
                        b_coeffs_trace_per_rec, spike_freq_trace_per_osc_per_rec,
                        t_spike_freq_trace_per_rec, spike_times_per_osc_per_rec,
                        thr_delays_per_osc, adjusting_factors_per_osc, scaling_factors,
                        step_size_adjusting_factor, step_size_scaling_factor):

        h5_file = h5py.File(outputfilename, 'w')
        print('Data written to: ', outputfilename)

        for rec_nr in range(n_recs_required):
            MyEMs = all_MyEMs[rec_nr]
            for osc_nr in range(N_osc):
                dset = h5_file.create_dataset("rec_nr{}/Osc{}/neuron_ids/exc".format(rec_nr, osc_nr), data=np.asarray(MyEMs['exc{}'.format(osc_nr)].neuron_ids), compression="gzip")
                dset = h5_file.create_dataset("rec_nr{}/Osc{}/neuron_ids/inh".format(rec_nr, osc_nr), data=np.asarray(MyEMs['inh{}'.format(osc_nr)].neuron_ids), compression="gzip")
                dset = h5_file.create_dataset("rec_nr{}/Osc{}/spiketimes/exc".format(rec_nr, osc_nr), data=np.asarray(MyEMs['exc{}'.format(osc_nr)].spike_times), compression="gzip")
                dset = h5_file.create_dataset("rec_nr{}/Osc{}/spiketimes/inh".format(rec_nr, osc_nr), data=np.asarray(MyEMs['inh{}'.format(osc_nr)].spike_times), compression="gzip")

                dset = h5_file.create_dataset("rec_nr{}/Osc{}/input_spike_freq_trace".format(rec_nr, osc_nr), data=spike_freq_trace_per_osc_per_rec[rec_nr][osc_nr], compression="gzip")
                dset = h5_file.create_dataset("rec_nr{}/Osc{}/input_spike_times".format(rec_nr, osc_nr), data=spike_times_per_osc_per_rec[rec_nr][osc_nr], compression="gzip")

            dset = h5_file.create_dataset("rec_nr{}/dt_run".format(rec_nr), data=[all_dt_runs[rec_nr]], compression="gzip")
            dset = h5_file.create_dataset("rec_nr{}/t_input_spike_freq_trace".format(rec_nr), data=t_spike_freq_trace_per_rec[rec_nr], compression="gzip")
            dset = h5_file.create_dataset("rec_nr{}/breathing_coefficient_trace".format(rec_nr), data=b_coeffs_trace_per_rec[rec_nr], compression="gzip")

        all_thr_delays = []
        all_adjusting_factors = []
        all_scaling_factors = []
        for osc_nr in range(N_osc):
            all_thr_delays.append(thr_delays_per_osc[osc_nr])
            all_adjusting_factors.append(adjusting_factors_per_osc[osc_nr])
            all_scaling_factors.append(scaling_factors[osc_nr])

        dset = h5_file.create_dataset("thr_delays_per_osc", data=all_thr_delays, compression="gzip")
        dset = h5_file.create_dataset("adjusting_factors_per_osc", data=all_adjusting_factors, compression="gzip")
        dset = h5_file.create_dataset("scaling_factors", data=all_scaling_factors, compression="gzip")
        dset = h5_file.create_dataset("step_size_adjusting_factor", data=[step_size_adjusting_factor], compression="gzip")
        dset = h5_file.create_dataset("step_size_scaling_factor", data=[step_size_scaling_factor], compression="gzip")

        h5_file.close()

