# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import json

class BiasSaver(object):

    def __init__(self, base_dir_biases):
        self.base_dir_biases = base_dir_biases

    def save_biases(self, dynapse_model, outputfilename):

        biases_all_groups = []
        for bias_group in dynapse_model.get_bias_groups():
            biases_one_group = {}
            for bias in bias_group.get_biases():
                biases_one_group[bias.get_bias_name()] = (bias.get_fine_value(), bias.get_coarse_value())
            biases_all_groups.append(biases_one_group)

        with open(outputfilename+'.json', 'w') as outfile:
            json.dump(biases_all_groups, outfile)

        print('Biases saved to {}.json.'.format(outputfilename.split('/')[-1]))
        return

    def load_biases(self, dynapse_model, inputfilename, verbose=True):

        with open(inputfilename, 'r') as infile:
            loaded_bias_groups = json.load(infile)

        for bias_group_nr, bias_group in enumerate(loaded_bias_groups):
            for bias_name, (fine_value, coarse_value) in bias_group.items():
                dynapse_model.get_bias_groups()[bias_group_nr].set_bias(bias_name, fine_value, coarse_value)

        if verbose:
            print('Biases restored!')

        return loaded_bias_groups

