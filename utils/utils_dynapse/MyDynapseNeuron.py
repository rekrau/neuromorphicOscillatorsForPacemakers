# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

class MyDynapseNeuron(object):
    ''' Class to help mapping neurons on the DYNAP-SE board (helps to map location on core to neuron id)'''
    def __init__(self, neuron_id_global=None, chip_id=None, core_id=None,
                 neuron_id_core=None, column_nr_core=None, row_nr_core=None):

        # properties
        self.neuron_id_global  = neuron_id_global

        self.chip_id = chip_id
        self.core_id = core_id
        self.neuron_id_core = neuron_id_core
        self.column_nr_core = column_nr_core
        self.row_nr_core   = row_nr_core

        # define constants
        self.neurons_per_row  = 16
        self.neurons_per_core = 256
        self.cores_per_chip   = 4
        self.neurons_per_chip = self.cores_per_chip * self.neurons_per_core

        # infer missing properties
        if self.neuron_id_global is None:
            if self.neuron_id_core is None:
                self.set_neuron_id_core_from_column_nr_row_nr()
            else:
                self.set_column_nr_row_nr_from_neuron_id_core()

            self.set_neuron_id_global()

        else:
            self.set_chip_core_neuron_id_core_from_neuron_id_global()
            self.set_column_nr_row_nr_from_neuron_id_core()

        self.set_neuron_id_chip()

    def set_neuron_id_chip(self):
        self.neuron_id_chip = self.neuron_id_core + self.core_id*self.neurons_per_core


    def set_neuron_id_global(self):
        self.neuron_id_global = (self.chip_id * self.neurons_per_chip) + \
                                (self.core_id * self.neurons_per_core) + self.neuron_id_core


    def set_neuron_id_core_from_column_nr_row_nr(self):
        self.neuron_id_core = self.row_nr_core*self.neurons_per_row + self.column_nr_core


    def set_column_nr_row_nr_from_neuron_id_core(self):
        self.row_nr_core    = self.neuron_id_core//self.neurons_per_row
        self.column_nr_core = self.neurons_per_core%self.neurons_per_row


    def set_chip_core_neuron_id_core_from_neuron_id_global(self):

        chip_id = self.neuron_id_global // self.neurons_per_chip
        if self.chip_id is None:
            self.chip_id = chip_id
        else:
            assert self.chip_id == chip_id, "Inconsistent chip ID"

        core_id = (self.neuron_id_global%self.neurons_per_chip)//self.neurons_per_core
        if self.core_id is None:
            self.core_id == core_id
        else:
            assert self.core_id == core_id, "Inconsistent core ID"

        neuron_id_core = ((self.neuron_id_global%self.neurons_per_chip)%self.neurons_per_core)
        if self.neuron_id_core is None:
            self.neuron_id_core = neuron_id_core
        else:
            assert self.neuron_id_core == neuron_id_core, "Inconsistent neuron ID core"

        self.set_column_nr_row_nr_from_neuron_id_core()
