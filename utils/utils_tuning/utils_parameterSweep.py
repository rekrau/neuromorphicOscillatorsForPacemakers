# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import matplotlib.pylab as plt
import h5py
import time

from neuromorphicOscillatorsForPacemakers.CPG_model.ActivationTimeExtractor import ActivationTimeExtractor
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrEvents import events_to_array, get_events_within_dt, split_events_into_subgroups, time_filter_events
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrSpikegen import get_one_fpga_event
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.DynapseOffSwitcher import DynapseOffSwitcher

ATE = ActivationTimeExtractor()


def isolate_oscillator(osc_nr_to_test, N_osc, core_ids_global_pop_exc, core_ids_global_pop_inh, dynapse_model, CtxDynapse):

    DynOff = DynapseOffSwitcher(CtxDynapse)

    for osc_nr in range(N_osc):
        bias_group_nr_exc = core_ids_global_pop_exc[osc_nr]
        bias_group_nr_inh = core_ids_global_pop_inh[osc_nr]
        DynOff.switch_off_specific_synpases(dynapse_model=dynapse_model, bias_group_nrs=[bias_group_nr_exc], syn_types=['fast_exc'], verbose=False)
        DynOff.switch_off_specific_synpases(dynapse_model=dynapse_model, bias_group_nrs=[bias_group_nr_inh], syn_types=['slow_inh'], verbose=False)

        if osc_nr != osc_nr_to_test:
            dynapse_model.get_bias_groups()[bias_group_nr_exc].set_bias('IF_DC_P', 0, 0)
            dynapse_model.get_bias_groups()[bias_group_nr_exc].set_bias('IF_TAU1_N', 255, 7)
            dynapse_model.get_bias_groups()[bias_group_nr_exc].set_bias('IF_THR_N', 0, 0)
            dynapse_model.get_bias_groups()[bias_group_nr_inh].set_bias('IF_TAU1_N', 255, 7)
            dynapse_model.get_bias_groups()[bias_group_nr_inh].set_bias('IF_THR_N', 0, 0)


def run_param_sweep_on_one_core(bias_name, all_linear_biases, dt_init, dt_run,
        neuron_ids_per_subgroup, core_id_to_sweep_biases, current_EventBuffer, dynapse_model,
        outputfilename, CtxDynapse):

    t_start = time.time()
    h5_file = h5py.File(outputfilename, 'w')
    n_biases = len(all_linear_biases)
    for nr, bias_current in enumerate(all_linear_biases):
        print('{}/{} biases done\r'.format(nr, n_biases))
        dynapse_model.get_bias_groups()[core_id_to_sweep_biases].set_linear_bias(bias_name, bias_current)
        events_all = get_events_within_dt(dt_init=dt_init, dt_run=dt_run,
                                          current_EventBuffer=current_EventBuffer, CtxDynapse=CtxDynapse, verbose=False)
        events_per_subgroup = split_events_into_subgroups(events_all, ids_per_subgroup=neuron_ids_per_subgroup)

        for subgroup_name in neuron_ids_per_subgroup.keys():
            neuron_ids, spiketimes = events_to_array(events_per_subgroup[subgroup_name])
            dset = h5_file.create_dataset("{}/I{}/{}/neuron_ids".format(bias_name, bias_current, subgroup_name),
                                          data=np.asarray(neuron_ids), compression="gzip")
            dset = h5_file.create_dataset("{}/I{}/{}/spiketimes".format(bias_name, bias_current, subgroup_name),
                                          data=np.asarray(spiketimes), compression="gzip")
            del neuron_ids, spiketimes

    h5_file.close()
    print('Duration:', time.time()-t_start)


def sweep_input_spike_rate_on_one_core(MyOscs, osc_nr_to_tune, input_spike_rates, dt_run, dt_spikegen_on,
                                           unit_mult, chip_ids_pop_exc, neuron_ids_per_subgroup, BuffEFs_per_osc,
                                           outputfilename, CtxDynapse):

    h5_file = h5py.File(outputfilename, 'w')
    for input_spike_rate in input_spike_rates:
        print(input_spike_rate)
        if input_spike_rate > 0:
            # Set and load fpga events
            isi_fpga = int(((input_spike_rate * 1e-6)**(-1)) / unit_mult)
            fpga_event = get_one_fpga_event(CtxDynapse=CtxDynapse, core_mask=5, target_chip=chip_ids_pop_exc[osc_nr_to_tune], virtual_neuron_id=1, isi_fpga=isi_fpga)
            MyOscs.spikegen.set_variable_isi(False)
            MyOscs.spikegen.preload_stimulus([fpga_event])
            MyOscs.spikegen.set_isi(isi_fpga)
            MyOscs.spikegen.set_repeat_mode(True)

            # clean buffer and reset time
            ignored_events = BuffEFs_per_osc[osc_nr_to_tune].get_events()  # req. to empty buffer
            del ignored_events
            CtxDynapse.dynapse.reset_timestamp()

            # start spikegen protocol
            MyOscs.spikegen.start()
            time.sleep(dt_spikegen_on)
            MyOscs.spikegen.stop()
            time.sleep(dt_run-dt_spikegen_on)

        else:
            # clean buffer and reset time
            ignored_events = BuffEFs_per_osc[osc_nr_to_tune].get_events()  # req. to empty buffer
            del ignored_events
            CtxDynapse.dynapse.reset_timestamp()
            time.sleep(dt_run)

        # collect dynapse events, and filter for dt to ensure that only events from within the dt_run are returned
        all_events = BuffEFs_per_osc[osc_nr_to_tune].get_events()
        all_events = time_filter_events(all_events=all_events, t_min=0, t_max=dt_run*1e6)

        events_per_subgroup = split_events_into_subgroups(all_events, ids_per_subgroup=neuron_ids_per_subgroup)
        for subgroup_name in neuron_ids_per_subgroup.keys():
            neuron_ids, spiketimes = events_to_array(events_per_subgroup[subgroup_name])
            dset = h5_file.create_dataset("rate{}/{}/neuron_ids".format(input_spike_rate, subgroup_name),
                                          data=np.asarray(neuron_ids), compression="gzip")
            dset = h5_file.create_dataset("rate{}/{}/spiketimes".format(input_spike_rate, subgroup_name),
                                          data=np.asarray(spiketimes), compression="gzip")
            del neuron_ids, spiketimes

    h5_file.close()
    return


def plot_avgISI_per_inputSpikeRate(inputfilename, input_spike_rates, dt_run, dt, tau, thr_exc, min_t_diff_t_thr):

    avg_isis_over_fine_values_first  = np.zeros_like(input_spike_rates)
    for counter, input_spike_rate in enumerate(input_spike_rates):
        neuron_ids_exc, spiketimes_exc = load_events_of_single_iteration(inputfilename, datasetname = "rate{}/exc/".format(input_spike_rate))
        A_exc, A_t_exc = ATE.get_average_activitiy_trace(spiketimes_exc, dt=dt, tau=tau, A_step=1., t_start=0, t_end=dt_run*1e6)
        t_thr_crossing_exc = np.asarray(ATE.get_t_threshold_crossing(A=A_exc, A_t=A_t_exc, thr=thr_exc, min_t_diff=min_t_diff_t_thr))
        if len(t_thr_crossing_exc)>1:
            avg_isis_over_fine_values_first[counter]  = np.diff(np.hstack(t_thr_crossing_exc).flatten())[0]

    fig = plt.figure()
    plt.plot(input_spike_rates, avg_isis_over_fine_values_first*1e-6,label='first')
    plt.xlabel('input spike rates')
    plt.ylabel('avg ISI (s)')

    return avg_isis_over_fine_values_first


def load_events_of_single_iteration(inputfilename, datasetname):
    with h5py.File(inputfilename, 'r') as f:
        neuron_ids = np.asarray(f[datasetname+'neuron_ids'][()], dtype=int)
        spiketimes = np.asarray(f[datasetname+'spiketimes'][()], dtype=int)
    return neuron_ids, spiketimes










