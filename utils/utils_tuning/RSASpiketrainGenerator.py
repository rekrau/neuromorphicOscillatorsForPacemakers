# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import random

from neuromorphicOscillatorsForPacemakers.CPG_model.ActivationTimeExtractor import ActivationTimeExtractor
from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.function_templates import exp_fct, neg_double_exp_decay
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrSpikegen import merge_separated_spike_trains_for_spikegen

ATE = ActivationTimeExtractor()


class RSASpiketrainGenerator(object):

    def __init__(self, G_a, G_b, G_c, offset_h):
        """
        # a = 7.01970271e-07, b = -1.82705418e-04, c = 4.49817096e-01
        :param G_a, G_b, G_c: (floats), parameters of 2-degree polynomial fitted on G(h) on sECG data
        """

        self.G_a = G_a
        self.G_b = G_b
        self.G_c = G_c

        self.offset_h = offset_h

    def G(self, b_coeff):
        """
        method mapping breathing coefficient to inhibitory input spike rate
        :param b_coeff: trace of breating coefficients
        :return: h, input spike rate
        """
        h = self.G_a*(b_coeff**2) + self.G_b*b_coeff + self.G_c
        h = np.clip(h, a_min=0, a_max=np.inf)
        return h


    def osc_inv(self, h_org, thr_delay, adjusting_factor, osc_inv_params, exp_constA):
        """ Osc_inv(c) --> function to map breathing_coeff to input spike frequency """
        h = np.clip(h_org - self.offset_h, a_min=0, a_max=np.inf)
        input_spike_frequency_trace = neg_double_exp_decay(x=h, a=osc_inv_params[0], b=osc_inv_params[1],
                                                           c=osc_inv_params[2], d=osc_inv_params[3])\
                                      + exp_fct(X=(h_org-thr_delay), a=exp_constA, k=adjusting_factor)

        input_spike_frequency_trace = np.clip(input_spike_frequency_trace, a_min=0., a_max=np.inf)

        return input_spike_frequency_trace


    def _get_input_spike_frequency_trace(self, b_coeff_trace, osc_inv_params,
                         thr_delay, adjusting_factor, exp_constA, scaling_factor=1.):
        h = self.G(b_coeff_trace)
        input_spike_freqs = self.osc_inv(h_org=h,
                                         thr_delay=thr_delay,
                                         adjusting_factor=adjusting_factor,
                                         osc_inv_params=osc_inv_params,
                                         exp_constA=exp_constA)
        return input_spike_freqs*float(scaling_factor)


    def _get_spike_times_from_spike_frequency_trace(self, spike_frequency_trace,
                                            t_spike_frequency_trace, seed_nr_poisson=None):
        # convert to spike train
        if seed_nr_poisson is not None:
            random.seed(seed_nr_poisson)

        dt_spikegen = np.diff(t_spike_frequency_trace[0:2])
        spike_times = []
        for time_step_nr in range(len(t_spike_frequency_trace)):
            spike_freq = spike_frequency_trace[time_step_nr]

            if (spike_freq > 0):
                spike_prob = spike_freq*dt_spikegen
                if spike_prob > random.random():
                    spike_times.append(t_spike_frequency_trace[time_step_nr])

        return spike_times


    def get_spike_train_set_up_info(self, t_start, t_end,
                                    ECG_trace, osc_inv_params_per_osc, thr_delays_per_osc,
                                    adjusting_factors_per_osc, exp_constAs, scaling_factors,
                                    seed_nr_poisson):

        dt_ecg = 1./ECG_trace.sampling_rate
        N_osc  = len(thr_delays_per_osc)

        idx_start = int(np.floor(t_start/dt_ecg))
        idx_end   = int(np.ceil(t_end/dt_ecg))
        current_t_spike_freq_trace = np.asarray(ECG_trace.t_rec[idx_start:idx_end]) - t_start
        current_b_coeffs_trace     = ECG_trace.breathing_coefficient_trace[idx_start:idx_end]

        # get new input spike frequency traces
        complete_input_spike_freq_trace_per_osc = []
        for osc_nr in range(N_osc):
            complete_input_spike_freq_trace_per_osc.append(self._get_input_spike_frequency_trace(b_coeff_trace=ECG_trace.breathing_coefficient_trace,
                                                                                        osc_inv_params=osc_inv_params_per_osc[osc_nr],
                                                                                        thr_delay=thr_delays_per_osc[osc_nr],
                                                                                        adjusting_factor=adjusting_factors_per_osc[osc_nr],
                                                                                        exp_constA=exp_constAs[osc_nr],
                                                                                        scaling_factor=scaling_factors[osc_nr]))

        current_spike_freq_trace_per_osc = {}
        for osc_nr in range(N_osc):
            current_spike_freq_trace_per_osc[osc_nr] = complete_input_spike_freq_trace_per_osc[osc_nr][idx_start:idx_end]

        spike_times_per_osc = {}
        for osc_nr in range(N_osc):
            spike_times_per_osc[osc_nr] = np.asarray(self._get_spike_times_from_spike_frequency_trace(
                spike_frequency_trace=current_spike_freq_trace_per_osc[osc_nr],
                t_spike_frequency_trace=current_t_spike_freq_trace,
                seed_nr_poisson=seed_nr_poisson))

        return current_b_coeffs_trace, current_spike_freq_trace_per_osc, current_t_spike_freq_trace, spike_times_per_osc


    def preload_dynapse_spikegen(self, spike_times_per_osc, spikegen, MyFPGAEventGen,
                                 virtual_neuron_ids, spikegen_core_mask_per_osc, spikegen_chip_ids_per_osc,
                                 add_readout_events=False, n_spikes_to_readout=1, readout_chip_id=None):
        """ method to generate FPGA events for RSA input and load them onto FPGA to later only start SpikeGen """
        # merge spikes of the different oscillators
        if len(spike_times_per_osc.keys())>1:
            all_spike_times, all_core_masks, all_target_chip_ids = merge_separated_spike_trains_for_spikegen(spike_trains=spike_times_per_osc,
                                                                                     core_mask_per_spike_train=spikegen_core_mask_per_osc,
                                                                                     target_chip_id_per_spike_train=spikegen_chip_ids_per_osc)
        else:
            osc_key = 0
            n_events = len(spike_times_per_osc[osc_key])
            all_spike_times     = spike_times_per_osc[osc_key]
            all_core_masks      = [spikegen_core_mask_per_osc[osc_key]]*n_events
            all_target_chip_ids = [spikegen_chip_ids_per_osc[osc_key]]*n_events

        # generate FPGA events
        n_events    = len(all_spike_times)
        fpga_events = MyFPGAEventGen.get_fpga_events(isis=[all_spike_times[0]]+list(np.diff(all_spike_times)),
                                                     main_core_masks=all_core_masks,
                                                     main_target_chips=all_target_chip_ids,
                                                     main_neuron_ids=virtual_neuron_ids*n_events,
                                                     verbose=False)

        if add_readout_events:
            fpga_events_readout_spikegen = MyFPGAEventGen.get_fpga_events(isis=[0.]*n_spikes_to_readout,
                                                                          main_core_masks=[15]*n_spikes_to_readout,
                                                                          main_target_chips=[readout_chip_id]*n_spikes_to_readout,
                                                                          main_neuron_ids=[virtual_neuron_ids[0]]*n_spikes_to_readout,
                                                                          verbose=False)
            all_fpga_events = fpga_events_readout_spikegen+fpga_events
        else:
            all_fpga_events = fpga_events

        assert len(all_fpga_events) <= MyFPGAEventGen.max_fpga_events, \
            'Too many events to send to FPGA ({} for maximally {} possible)'.format(len(fpga_events), MyFPGAEventGen.max_fpga_events)

        spikegen.set_variable_isi(True)
        spikegen.preload_stimulus(all_fpga_events)
        spikegen.set_repeat_mode(False)
