# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

from neuromorphicOscillatorsForPacemakers.utils.utils_visualization.OscillatorPlotter import OscillatorPlotter
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrSpikegen import get_events_incl_spikegen_onset_delay_readout

OPlt = OscillatorPlotter()

def run_RSACPG_over_one_rec(t_start, t_end,

                            ECG_trace,
                            osc_inv_params_per_osc, thr_delays_per_osc,
                            adjusting_factors_per_osc, exp_constAs, scaling_factors,

                            current_EventBuffer, BuffEF_spikegen_readout,
                            spikegen,
                           virtual_neuron_ids, spikegen_core_mask_per_osc, spikegen_chip_ids_per_osc,

                            MyRSASG, MyFPGAEventGen,

                            CtxDynapse,
                            seed_nr_poisson,
                            add_readout_events=False, n_spikes_to_readout=1, readout_chip_id=None,
                            return_all_data=False, require_user_ok=False):

    dt_run = (t_end - t_start)
    current_b_coeffs_trace, current_spike_freq_trace_per_osc, \
    current_t_spike_freq_trace, spike_times_per_osc = MyRSASG.get_spike_train_set_up_info(t_start=t_start, t_end=t_end,
                                                                   ECG_trace=ECG_trace,
                                                                   osc_inv_params_per_osc=osc_inv_params_per_osc,
                                                                   thr_delays_per_osc=thr_delays_per_osc,
                                                                   adjusting_factors_per_osc=adjusting_factors_per_osc,
                                                                   exp_constAs=exp_constAs, scaling_factors=scaling_factors,
                                                                   seed_nr_poisson=seed_nr_poisson)

    MyRSASG.preload_dynapse_spikegen(spike_times_per_osc=spike_times_per_osc,
                                   spikegen=spikegen,
                                   MyFPGAEventGen=MyFPGAEventGen,
                                   virtual_neuron_ids=virtual_neuron_ids,
                                   spikegen_core_mask_per_osc=spikegen_core_mask_per_osc,
                                   spikegen_chip_ids_per_osc=spikegen_chip_ids_per_osc,
                                   add_readout_events=add_readout_events, n_spikes_to_readout=n_spikes_to_readout, readout_chip_id=readout_chip_id)

    # collect events and process
    if require_user_ok:
        input('Send input spikes?')

    all_events, all_events_readout = get_events_incl_spikegen_onset_delay_readout(BuffEF=current_EventBuffer,
                                                                                  BuffEF_spikegen_readout=BuffEF_spikegen_readout,
                                                                                  dt_run=dt_run,
                                                                                  my_spikegen=spikegen,
                                                                                  CtxDynapse=CtxDynapse)

    if return_all_data:
        return all_events, all_events_readout, \
                current_b_coeffs_trace, current_spike_freq_trace_per_osc, \
                current_t_spike_freq_trace, spike_times_per_osc
    else:
        return all_events, all_events_readout
