# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


class CurveFitter(object):
    """ Class to fit functions based on recorded DYNAP-SE events """

    def _is_nan(self, x):
        return (x != x)

    def remove_nans(self, xdata, ydata, verbose=True, show_plot=True, xlabel='', ylabel=''):
        """remove nans from x and y data to improve fitting """
        n_nans = 0
        xdata_filtered, ydata_filtered = [], []
        for x, y in zip(xdata, ydata):
            if not self._is_nan(x) and not self._is_nan(y):
                xdata_filtered.append(x)
                ydata_filtered.append(y)
            else:
                n_nans += 1

        if verbose:
            print('{} nans found'.format(n_nans))

        xdata_filtered = np.asarray(xdata_filtered)
        ydata_filtered = np.asarray(ydata_filtered)

        if show_plot:
            plt.plot(xdata_filtered, ydata_filtered)
            plt.title('curve after removing nans')
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)

        return xdata_filtered, ydata_filtered


    def balance_dataset(self, xdata, ydata, bin_size, x_range=None, samples_per_bin=5, verbose=True, seed=None):
        """ balance dataset to have roughly the same number of samples in different parameter regions """

        if seed is not None:
            np.random.seed(seed)

        if x_range is None:
            x_min, x_max = np.min(xdata.flatten()), np.max(xdata.flatten())
        else:
            x_min, x_max = x_range[0], x_range[1]

        xdata_balanced, ydata_balanced = [], []
        for bin_start in np.arange(x_min, x_max, bin_size):
            bin_end = bin_start + bin_size
            indices_in_bin = np.where(np.logical_and(xdata>bin_start, xdata<bin_end))[0]

            if len(indices_in_bin)>0:

                if len(indices_in_bin) < samples_per_bin:
                    add_sample_multiple_times=True
                elif len(indices_in_bin) >= samples_per_bin:
                    add_sample_multiple_times = False

                indices_to_add = np.random.choice(indices_in_bin, replace=add_sample_multiple_times, size=samples_per_bin)
                for idx_to_add in indices_to_add:
                    xdata_balanced.append(xdata[idx_to_add])
                    ydata_balanced.append(ydata[idx_to_add])

        if verbose:
            print('n samples: ', len(xdata_balanced), 'out of', len(xdata))
            plt.hist(xdata_balanced, bins=np.arange(x_min, x_max, bin_size))

        return np.asarray(xdata_balanced), np.asarray(ydata_balanced)


    def _get_legend_label_for_func(self, n_params_in_func, popt):
        if n_params_in_func == 2:
            legend_label = 'fit: a=%5.3f, b=%5.3f' % tuple(popt)
        elif n_params_in_func == 3:
            legend_label = 'fit: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt)
        elif n_params_in_func == 4:
            legend_label = 'fit: a=%5.3f, b=%5.3f, c=%5.3f, d=%5.3f' % tuple(popt)
        elif n_params_in_func == 5:
            legend_label = 'fit: a=%5.3f, b=%5.3f, c=%5.3f, d=%5.3f, e=%5.3f' % tuple(popt)

        return legend_label


    def fit_curve_and_plot(self, xdata, ydata, func, verbose=True, show_plot=True, xlabel='', ylabel='', n_params_in_func=4, ax=None):
        if ax is None:
            fig, ax = plt.subplots()

        popt, pcov = curve_fit(func, xdata.flatten(), ydata.flatten(), )
        if verbose:
            print(popt)

        if show_plot:
            ax.plot(xdata, ydata, 'b.', label='data')
            ax.plot(xdata, func(xdata, *popt), 'r', label=self._get_legend_label_for_func(n_params_in_func, popt))
            ax.set_ylabel(ylabel)
            ax.set_xlabel(xlabel)
            ax.legend()

        return popt, pcov


    def get_r_square_of_fit(self, y, y_fit):
        ss_res = np.sum((y - y_fit) ** 2)
        ss_tot = np.sum((y - np.mean(y)) ** 2)
        r2 = 1 - (ss_res / ss_tot)
        return r2
