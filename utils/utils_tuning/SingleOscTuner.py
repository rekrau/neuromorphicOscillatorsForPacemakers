# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np

from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.BiasValueConverter import BiasValueConverter
from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.utils_hardwareTuning import increase_bias_value, decrease_bias_value
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrEvents import get_events_within_dt
from neuromorphicOscillatorsForPacemakers.utils.utils_visualization.OscillatorPlotter import OscillatorPlotter

BVC = BiasValueConverter()
OPlt = OscillatorPlotter()

class SingleOscTuner(object):
    '''' class to tune frequency of single oscillator '''

    def __init__(self,
                 dt_init=5.,
                 dt_run=10.,
                 min_diff_to_change_DC=50000,
                 step_size_DC=10,
                 step_size_W=5,
                 thr_exc=8.,
                 thr_inh=1.,
                 dt=10.,
                 tau=0.05*1e6,
                 snapshot_every=5,
                 CtxDynapse=None):

        self.dt_init = dt_init
        self.dt_run = dt_run
        self.min_diff_to_change_DC = min_diff_to_change_DC
        self.step_size_DC = step_size_DC
        self.step_size_W = step_size_W
        self.thr_exc = thr_exc
        self.thr_inh = thr_inh
        self.dt = dt
        self.tau = tau
        self.snapshot_every = snapshot_every
        self.CtxDynapse = CtxDynapse

    def _adjust_DC(self, diff_to_target_delay, current_delay_mean, DC_fine_value, DC_coarse_value):

        if diff_to_target_delay > 0 or current_delay_mean != current_delay_mean:  # too slow / long
            for i in range(self.step_size_DC):
                DC_fine_value, DC_coarse_value = increase_bias_value(fine_value=DC_fine_value,
                                                                     coarse_value=DC_coarse_value)

        elif diff_to_target_delay < 0:  # too fast / short
            for i in range(self.step_size_DC):
                DC_fine_value, DC_coarse_value = decrease_bias_value(fine_value=DC_fine_value,
                                                                     coarse_value=DC_coarse_value)

        return DC_fine_value, DC_coarse_value


    def _adjust_W(self, diff_to_target_delay, W_exc2inh_fine_value, W_exc2inh_coarse_value):

        if diff_to_target_delay > 0:  # too slow / long
            for i in range(self.step_size_W):
                W_exc2inh_fine_value, W_exc2inh_coarse_value = increase_bias_value(fine_value=W_exc2inh_fine_value,
                                                                                   coarse_value=W_exc2inh_coarse_value)
        elif diff_to_target_delay < 0:  # too fast / short
            for i in range(self.step_size_W):
                W_exc2inh_fine_value, W_exc2inh_coarse_value = decrease_bias_value(fine_value=W_exc2inh_fine_value,
                                                                                   coarse_value=W_exc2inh_coarse_value)

        return W_exc2inh_fine_value, W_exc2inh_coarse_value


    def switch_off_other_oscs(self, current_osc_nr, N_osc, core_ids_global_pop_exc,
                              dyn_params_SingleOsc_EXC, dynapse_model):
        # set tau of not tuned oscillators very high, other to default to avoid interference
        for osc_nr_internal in range(N_osc):
            if osc_nr_internal == current_osc_nr:
                biases = dyn_params_SingleOsc_EXC[current_osc_nr]
                dynapse_model.get_bias_groups()[core_ids_global_pop_exc[osc_nr_internal]].set_bias('IF_TAU1_N', biases['IF_TAU1_N'][0], biases['IF_TAU1_N'][1])
            else:
                dynapse_model.get_bias_groups()[core_ids_global_pop_exc[osc_nr_internal]].set_bias('IF_TAU1_N', 255, 7)
        return


    def get_mean_and_std_delay(self, current_EventBuffer, neuron_ids_global_pop_exc, neuron_ids_global_pop_inh):

        all_events = get_events_within_dt(dt_init=self.dt_init, dt_run=self.dt_run, current_EventBuffer=current_EventBuffer,
                                          CtxDynapse=self.CtxDynapse, verbose=False)

        MyEMs = OPlt.events_to_EMs_of_coupled_oscs(all_events=all_events, N_osc=1,
                                                   neuron_ids_global_pops_exc=[neuron_ids_global_pop_exc],
                                                   neuron_ids_global_pops_inh=[neuron_ids_global_pop_inh])

        MySVMs = OPlt.EMs_to_SVMs_Atraces_of_coupled_osc(MyEMs,
                                                         N_osc=1,
                                                         dt=self.dt,
                                                         tau=self.tau)

        t_thr_crossings = OPlt.SVMs_Atraces_to_t_threshold_crossings_of_coupled_oscs(MySVMs=MySVMs,
                                                                                     N_osc=1,
                                                                                     thr_exc=self.thr_exc,
                                                                                     thr_inh=self.thr_inh,
                                                                                     verbose=False)

        current_delay_mean = np.mean(np.diff(t_thr_crossings['exc0'].flatten()))
        current_delay_std  = np.std(np.diff(t_thr_crossings['exc0'].flatten()))

        return current_delay_mean, current_delay_std


    def tune_freq_of_single_osc(self, target_delay, delay_tolerance,
                                    DC_fine_value, DC_coarse_value,
                                    W_exc2inh_fine_value, W_exc2inh_coarse_value,
                                    dynapse_model, bias_group_nr_exc, bias_group_nr_inh,
                                    neuron_ids_global_pop_exc, neuron_ids_global_pop_inh,
                                    current_EventBuffer):

        counter = 0
        while True:
            counter += 1
            dynapse_model.get_bias_groups()[bias_group_nr_exc].set_bias('IF_DC_P', DC_fine_value, DC_coarse_value)
            dynapse_model.get_bias_groups()[bias_group_nr_inh].set_bias('PS_WEIGHT_EXC_F_N', W_exc2inh_fine_value, W_exc2inh_coarse_value)
            current_delay_mean, current_delay_std = self.get_mean_and_std_delay(current_EventBuffer=current_EventBuffer,
                                                                                neuron_ids_global_pop_exc=neuron_ids_global_pop_exc,
                                                                                neuron_ids_global_pop_inh=neuron_ids_global_pop_inh)

            # snapshots
            if counter%self.snapshot_every == 0:
                print('\t \t SNAPSHOT (It {}): delay: {}+-{} for Weight: ({}, {}), DC: ({}, {})'.format(counter,
                        current_delay_mean, current_delay_std,
                        W_exc2inh_fine_value, W_exc2inh_coarse_value, DC_fine_value, DC_coarse_value))


            diff_to_target_delay = current_delay_mean - target_delay
            if np.abs(diff_to_target_delay) < delay_tolerance:
                print('\t FINAL delay: {} +- {} for Weight: ({}, {}), DC: ({}, {})'.format(
                        current_delay_mean, current_delay_std,
                        W_exc2inh_fine_value, W_exc2inh_coarse_value, DC_fine_value, DC_coarse_value))
                break

            else:
                # change DC
                I_W = BVC.get_current(fine_value=W_exc2inh_fine_value,
                                      coarse_value=W_exc2inh_coarse_value)
                if np.abs(diff_to_target_delay) > self.min_diff_to_change_DC or current_delay_mean != current_delay_mean or I_W > BVC.get_current(fine_value=230, coarse_value=7):
                    DC_fine_value, DC_coarse_value = self._adjust_DC(diff_to_target_delay=diff_to_target_delay,
                                                                        current_delay_mean=current_delay_mean,
                                                                        DC_fine_value=DC_fine_value,
                                                                        DC_coarse_value=DC_coarse_value)

                # change weight
                else:
                    W_exc2inh_fine_value, W_exc2inh_coarse_value = self._adjust_W(diff_to_target_delay=diff_to_target_delay,
                                                                                   W_exc2inh_fine_value=W_exc2inh_fine_value,
                                                                                   W_exc2inh_coarse_value=W_exc2inh_coarse_value)
