# SPDX-License-Identifier: MIT
# Copyright (c) 2021 University of Zurich

import numpy as np
import matplotlib.pylab as plt

from neuromorphicOscillatorsForPacemakers.CPG_model.ActivationTimeExtractor import ActivationTimeExtractor
from neuromorphicOscillatorsForPacemakers.utils.utils_visualization.OscillatorPlotter import OscillatorPlotter
from neuromorphicOscillatorsForPacemakers.utils.utils_dynapse.utils_CtxctrSpikegen import correct_spiketimes_for_spike_generator_onset_delay
from neuromorphicOscillatorsForPacemakers.utils.utils_tuning.utils_RSARestoration import run_RSACPG_over_one_rec

OPlt = OscillatorPlotter()
ATE = ActivationTimeExtractor()


class RSAOscSummaryPlotter(object):
    """ Class to show activity of coupled oscillators when provided with
        inhibitory input to model RSA """

    def __init__(self, N_osc, N_exc, N_inh,
                 neuron_ids_global_pops_exc, neuron_ids_global_pops_inh,
                 ECG_trace, osc_inv_params_per_osc,
                 n_recs_required, max_rec_duration,
                 current_EventBuffer, BuffEF_spikegen_readout, spikegen, seed_nr_poisson,
                 virtual_neuron_ids, spikegen_core_mask_per_osc, spikegen_chip_ids_per_osc,
                 n_spikes_to_readout, readout_chip_id, readout_core_mask,
                 MyRSASG, MyFPGAEventGen, CtxDynapse):

        self.CtxDynapse = CtxDynapse

        self.N_osc = N_osc
        self.N_exc = N_exc
        self.N_inh = N_inh

        self.neuron_ids_global_pops_exc = neuron_ids_global_pops_exc
        self.neuron_ids_global_pops_inh = neuron_ids_global_pops_inh

        self.ECG_trace = ECG_trace
        self.osc_inv_params_per_osc = osc_inv_params_per_osc

        self.n_recs_required  = n_recs_required
        self.max_rec_duration = max_rec_duration

        self.current_EventBuffer     = current_EventBuffer
        self.BuffEF_spikegen_readout = BuffEF_spikegen_readout
        self.spikegen                = spikegen

        self.seed_nr_poisson            = seed_nr_poisson
        self.virtual_neuron_ids         = virtual_neuron_ids
        self.spikegen_core_mask_per_osc = spikegen_core_mask_per_osc
        self.spikegen_chip_ids_per_osc  = spikegen_chip_ids_per_osc
        self.n_spikes_to_readout        = n_spikes_to_readout
        self.readout_chip_id            = readout_chip_id
        self.readout_core_mask          = readout_core_mask

        self.MyRSASG        = MyRSASG
        self.MyFPGAEventGen = MyFPGAEventGen


    def collect_recordings_RSA_CPG(self,
                                   thr_exc, thr_inh, dt, tau,
                                   thr_delays_per_osc,
                                   adjusting_factors_per_osc,
                                   exp_constAs,
                                   scaling_factors,
                                   require_user_ok=False):

        all_events_over_recs         = []
        all_events_readout_over_recs = []

        b_coeffs_trace_per_rec           = []
        spike_freq_trace_per_osc_per_rec = []
        t_spike_freq_trace_er_rec        = []
        spike_times_per_osc_per_rec      = []

        MyEMs_per_rec  = []
        MySVMs_per_rec = []
        t_thrs_per_rec = []

        all_dt_runs = np.zeros(self.n_recs_required)
        for rec_nr in range(self.n_recs_required):

            t_start = rec_nr * self.max_rec_duration
            t_end   = np.min((t_start + self.max_rec_duration, self.ECG_trace.t_rec[-1]))
            all_dt_runs[rec_nr] = (t_end - t_start)

            all_events, all_events_readout, \
            current_b_coeffs_trace, current_spike_freq_trace_per_osc, \
            current_t_spike_freq_trace, spike_times_per_osc = run_RSACPG_over_one_rec(t_start=t_start,
                                                                                      t_end=t_end,
                                                                                      ECG_trace=self.ECG_trace,
                                                                                      osc_inv_params_per_osc=self.osc_inv_params_per_osc,
                                                                                      thr_delays_per_osc=thr_delays_per_osc,
                                                                                      adjusting_factors_per_osc=adjusting_factors_per_osc,
                                                                                      exp_constAs=exp_constAs,
                                                                                      scaling_factors=scaling_factors,
                                                                                      current_EventBuffer=self.current_EventBuffer,
                                                                                      BuffEF_spikegen_readout = self.BuffEF_spikegen_readout,
                                                                                      spikegen = self.spikegen,

                                                                                      virtual_neuron_ids=self.virtual_neuron_ids,
                                                                                      spikegen_core_mask_per_osc=self.spikegen_core_mask_per_osc,
                                                                                      spikegen_chip_ids_per_osc=self.spikegen_chip_ids_per_osc,

                                                                                      MyRSASG=self.MyRSASG,
                                                                                      MyFPGAEventGen=self.MyFPGAEventGen,

                                                                                      CtxDynapse=self.CtxDynapse,
                                                                                      seed_nr_poisson=self.seed_nr_poisson,
                                                                                      add_readout_events=True,
                                                                                      n_spikes_to_readout=self.n_spikes_to_readout,
                                                                                      readout_chip_id=self.readout_chip_id,
                                                                                      # readout_core_mask=self.readout_core_mask,
                                                                                      return_all_data=True,
                                                                                      require_user_ok=require_user_ok)


            MyEMs = OPlt.events_to_EMs_of_coupled_oscs(all_events=all_events,
                                                       N_osc=self.N_osc,
                                                       neuron_ids_global_pops_exc=self.neuron_ids_global_pops_exc,
                                                       neuron_ids_global_pops_inh=self.neuron_ids_global_pops_inh)
            correct_spiketimes_for_spike_generator_onset_delay(all_events_readout=all_events_readout, MyEMs_recordings=MyEMs)
            MySVMs = OPlt.EMs_to_SVMs_Atraces_of_coupled_osc(MyEMs, N_osc=self.N_osc, dt=dt, tau=tau)
            t_thrs = OPlt.SVMs_Atraces_to_t_threshold_crossings_of_coupled_oscs(MySVMs=MySVMs, N_osc=self.N_osc,
                                                                                thr_exc=thr_exc, thr_inh=thr_inh, verbose=False)

            all_events_over_recs.append(all_events)
            all_events_readout_over_recs.append(all_events_readout)

            b_coeffs_trace_per_rec.append(current_b_coeffs_trace)
            spike_freq_trace_per_osc_per_rec.append(current_spike_freq_trace_per_osc)
            t_spike_freq_trace_er_rec.append(current_t_spike_freq_trace)
            spike_times_per_osc_per_rec.append(spike_times_per_osc)

            MyEMs_per_rec.append(MyEMs)
            MySVMs_per_rec.append(MySVMs)
            t_thrs_per_rec.append(t_thrs)

        return all_events_over_recs, all_events_readout_over_recs, \
               b_coeffs_trace_per_rec, spike_freq_trace_per_osc_per_rec, \
               t_spike_freq_trace_er_rec, spike_times_per_osc_per_rec, \
               MyEMs_per_rec, MySVMs_per_rec, t_thrs_per_rec, all_dt_runs


    def plot_coupled_RSA_oscillator_summary(self, MyEMs, dt_run,
                                            current_spike_freq_trace_per_osc,
                                            current_t_spike_freq_trace,
                                            current_b_coeffs_trace, fontsize_label=16):
        # plotting
        fig, axes = plt.subplots(nrows=self.N_osc+2, figsize=(40,4+4*self.N_osc), sharex=True)
        RP = OPlt.get_rasterplot_of_coupled_oscs(MyEMs=MyEMs, N_osc=self.N_osc,
                                                 N_exc=self.N_exc, N_inh=self.N_inh,
                                                 dt_run=dt_run, squeeze_neuron_ids=True, subfig=axes[0])

        min_freqs, max_freqs = [], []
        for osc_nr in range(self.N_osc):
            min_freqs.append(np.min(current_spike_freq_trace_per_osc[osc_nr]))
            max_freqs.append(np.max(current_spike_freq_trace_per_osc[osc_nr]))
            OPlt.plot_stim_trace(stim_trace=current_spike_freq_trace_per_osc[osc_nr], t_stim_trace=current_t_spike_freq_trace*1e6,
                                 xlabel='time (us)', ylabel='stimulus osc {}'.format(osc_nr), subfig=axes[osc_nr+1], fontsize=fontsize_label)

        for osc_nr in range(self.N_osc):
            axes[osc_nr+1].set_ylim(np.min(min_freqs), np.max(max_freqs))

        axes[-1].plot(current_t_spike_freq_trace*1e6, current_b_coeffs_trace)
        axes[-1].set_xlabel('time (us)', fontsize=fontsize_label)
        axes[-1].set_ylabel('breath. coefficient C', fontsize=fontsize_label)
        plt.tight_layout()
        plt.show()

    def get_G_fit_for_DYNAPSE(self, osc_nr_to_show,
                              all_t_thrs_per_rec, b_coeffs_trace_per_rec, figsize=None):
        """ plot relation of oscillation period to average breathing coefficient
            for coupled oscillators on DYNAP-SE when stimulated with inhibitory
            input signals based on respiration signal """
        dt_ecg = 1./self.ECG_trace.sampling_rate

        if figsize is None:
            fig, ax = plt.subplots()
        else:
            fig, ax = plt.subplots(figsize=figsize)
        osc_nr = osc_nr_to_show
        pop_name_exc = 'exc'+str(int(osc_nr))
        for rec_nr in range(self.n_recs_required):
            rpeaks_indices = np.asarray(np.ceil(np.hstack(all_t_thrs_per_rec[rec_nr][pop_name_exc].flatten())*1e-6/dt_ecg), dtype=int)
            breathing_coeffs_dyn, r_peak_delays_dyn = self.ECG_trace.get_dataset_breathing_coeff_vs_r_peak_delays(rpeaks_indices=rpeaks_indices,
                                                                                                                  breathing_coefficient_trace=b_coeffs_trace_per_rec[rec_nr], dt=dt_ecg)

            plt.scatter(breathing_coeffs_dyn, r_peak_delays_dyn, color='b', label='rec nr: {}, Osc {}'.format(rec_nr, osc_nr), s=10)
            plt.scatter(breathing_coeffs_dyn, self.MyRSASG.G(breathing_coeffs_dyn), marker='*', s=50, color='r')

        breathing_coeffs, r_peak_delays = self.ECG_trace.get_dataset_breathing_coeff_vs_r_peak_delays(rpeaks_indices=self.ECG_trace.rpeaks_indices,
                                                                                                      breathing_coefficient_trace=self.ECG_trace.breathing_coefficient_trace, dt=dt_ecg)
        plt.scatter(breathing_coeffs, r_peak_delays, s=5, color='orange')
        plt.xlabel('breathing coefficient')
        plt.ylabel('r peak delay')
